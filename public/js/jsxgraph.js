var board = JXG.JSXGraph.initBoard('fig_one', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

var array = [
  [0, -1],
  [1, 2],
  [2, 5],
  [3, 8],
  [-1, -4],
  [-2, -7],
  [-3, -10]
];

for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: '',
    size: 1
  });
}

var board = JXG.JSXGraph.initBoard('fig_two', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

board.create('line', [
  [-3, -10],
  [3, 8]
]);


var board = JXG.JSXGraph.initBoard('fig_five', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

board.create('line', [
  [-1, -1],
  [1, 1]
]);

var array = [
  [15, 10],
  [12, 15],
  [7, 4],
  [0, 5],
  [-2, -5],
  [-4, -8],
  [-8, -11]
];
for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: '',
    size: 1
  });

  board.create('line', [
    [array[i][0], array[i][1]],
    [array[i][0], array[i][0]]
  ], {
    straightFirst: false,
    straightLast: false,
    strokeColor: '#954001',
    strokeWidth: 2,
    dash: 2
  });
}

var board = JXG.JSXGraph.initBoard('fig_six', {
  boundingbox: [-250, 250, 250, -250],
  axis: true
});

board.create('line', [
  [0, -6],
  [2, -1]
]);

var array = MakeNoisyData();
for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: '',
    size: 1
  });
}

var board = JXG.JSXGraph.initBoard('fig_seven', {
  boundingbox: [-2, 8, 8, -2],
  axis: true
});

var array = [
  [5, 7],
  [1, 3]
];

var names = ['A', 'B'];

for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: names[i],
    size: 1
  });
}

var lineStyle = {
  straightFirst: false,
  straightLast: false,
  strokeColor: '#954001',
  strokeWidth: 2,
  dash: 2
};

board.create('line', [
  [array[0][0], array[0][1]],
  [array[1][0], array[1][1]]
], lineStyle);

lineStyle['strokeColor'] = 'green';
board.create('line', [
  [array[0][0], array[0][1]],
  [array[0][0], array[1][1]]
], lineStyle);

lineStyle['strokeColor'] = 'blue';
board.create('line', [
  [array[1][0], array[1][1]],
  [array[0][0], array[1][1]]
], lineStyle);

var normalDist = JXG.JSXGraph.initBoard('fig_eight', {
  boundingbox: [-2, 6, 8, -2],
  axis: true
});

var dataX = [];
var sigma = [0.5, 1, 1];
var mu = [3, 4, 5];
var color = ['red', 'orange', 'blue']
for (i = 0; i < 8000; i++) {
  dataX.push(i * 0.1);
}

for (i = 0; i < 3; i++) {
  normalDist.create('curve', [dataX, function(x) {
    return (1 / sigma[i] * (Math.sqrt(2 * Math.PI))) * Math.exp(-0.5 * Math.pow(((x - mu[i]) / sigma[i]), 2));
  }], {
    strokeColor: color[i],
    strokeWidth: 2
  });
}

var logNormalDist = JXG.JSXGraph.initBoard('fig_nine', {
  boundingbox: [-1, 5, 5, -1],
  axis: true
});

var dataX = [];
var sigma = [0.25, 0.5, 1];
var mu = [0, 0, 0];
for (i = 0; i < 8000; i++) {
  dataX.push(i * 0.01);
}
for (i = 0; i < 3; i++) {
  logNormalDist.create('curve', [dataX, function(x) {
    return (1 / x * sigma[i] * (Math.sqrt(2 * Math.PI))) * Math.exp(-0.5 * Math.pow(((Math.log(x) - mu[i]) / sigma[i]), 2));
  }], {
    strokeColor: color[i],
    strokeWidth: 2
  });
}


function MakeNoisyData() {

  var err, setX;
  var setData = [];
  for (var i = 0; i < 50; i++) {
    err = Math.floor(Math.random() * 61) - 30;
    setX = Math.floor(Math.random() * 161) - 80;
    setData.push([setX, (2.5 * setX) - 6 + err]);
  }

  return setData;
}
